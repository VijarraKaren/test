<?php
require_once('class_bd.php');
require_once('class_web.php');
require_once('class_tabla.php');

class reparto{

	private $pagina;
	private $formulario;
	private $fecha;
	private $nombre ;
	private $apellido;
	private $direccion;
	private $horario;
	private $db;

	public function __construct(){
		$this->db = new database;

	}

	public function crear_form($id="")
	{
		$this->formulario = new pagina_Web_formulario("Listado de Repartos");		
 
		if ($id <> "") {
 			$this->select_id($id);
		}

		$this->formulario->cabecera();
		$this->formulario->cuerpo();
		if ($id <> "") {
		     $this->formulario->formulario_inicio("index_page.php?id=$id") ;
			}
		else {
		   $this->formulario->formulario_inicio("index_page.php") ;
		}
		$this->formulario->formulario_caja_texto("Fecha","fecha", "X",$this->fecha) ;
		$this->formulario->formulario_caja_texto("Nombre","nombre", "X",$this->nombre) ;	
		$this->formulario->formulario_caja_texto("Apellido","apellido","X", $this->apellido);
		$this->formulario->formulario_caja_texto("Direccion","direccion","X", $this->direccion);

		$this->formulario->form_radio("horario", "Mañana", "mañana", "checked");
        $this->formulario->form_radio("horario", "Tarde", "tarde");
	
		$this->formulario->formulario_boton("Registrar");
		$this->formulario->formulario_fin();
		$this->formulario->pie();

	}

	public function select_id($id){
		
		$result = $this->db->query("SELECT fecha, nombre, apellido, direccion,horario FROM repartos WHERE id = $id"); 

		if  (!empty($result)) {
			foreach ($result as $row) {
				$this->fecha=$row->fecha;
		  	   $this->nombre = $row->nombre;
		       $this->apellido = $row->apellido;
		       $this->direccion = $row->direccion;  
			   $this->horario = $row->horario;  
		     }  
		    }
	}

	public function insert($fecha, $nombre,$apellido,$direccion,$horario){
		$query = "INSERT INTO repartos(fecha,nombre,apellido,direccion,horario)
                   VALUES ('$fecha','$nombre','$apellido','$direccion','$horario')";
                    
		$result = $this->db->execute_query(utf8_encode($query));
	}

	public function update($id,$fecha,$nombre,$apellido,$direccion,$horario){
	 $query = "UPDATE repartos
	             SET fecha= '$fecha', 
				 	nombre   = '$nombre',
	                 apellido = '$apellido',
	                 direccion  = '$direccion',
					 horario  = '$horario'                 
                 WHERE id = $id";
                    
		$result = $this->db->execute_query(utf8_encode($query));
	}


	public function delete($id){
		$result = $this->db->execute_query(utf8_encode("DELETE FROM repartos WHERE id = $id"));
	}

	public function mostrar_tabla(){
		$this->pagina = new pagina_Web("Listado de Repartos");
		$this->pagina->cabecera();
		$this->pagina->cuerpo();

		echo "<a href='page.php' class='btn btn-secondary btn-lg btn-block'>Agregar reparto </a></div>";
		
		$result_select = $this->db->query("SELECT * FROM repartos"); 
		
		if  (!empty($result_select)) {

			$filas = count($result_select) + 1;
			$tabla1=new tabla($filas,7);
			$i = 1;
			$tabla1->cargar($i,1,"Borrar");
			$tabla1->cargar($i,2,"Editar");
			$tabla1->cargar($i,3,"Fecha");
			$tabla1->cargar($i,4,"Nombre");
			$tabla1->cargar($i,5,"Apellido");
			$tabla1->cargar($i,6,"Direccion");			
			$tabla1->cargar($i,7,"Horario");
			foreach ($result_select as $row) {
				$i++;
			
			$borrar =  "<a href='delete.php?id=$row->id'>" .  
						'<svg class="bi bi-trash" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
						  <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"></path>
						  <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"></path>
						</svg>' .  "</a>";
			$tabla1->cargar($i,1,$borrar);
			$editar = "<a href='page.php?id=$row->id'>" . 
							'<svg class="bi bi-pencil" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
							  <path fill-rule="evenodd" d="M11.293 1.293a1 1 0 0 1 1.414 0l2 2a1 1 0 0 1 0 1.414l-9 9a1 1 0 0 1-.39.242l-3 1a1 1 0 0 1-1.266-1.265l1-3a1 1 0 0 1 .242-.391l9-9zM12 2l2 2-9 9-3 1 1-3 9-9z"></path>
							  <path fill-rule="evenodd" d="M12.146 6.354l-2.5-2.5.708-.708 2.5 2.5-.707.708zM3 10v.5a.5.5 0 0 0 .5.5H4v.5a.5.5 0 0 0 .5.5H5v.5a.5.5 0 0 0 .5.5H6v-1.5a.5.5 0 0 0-.5-.5H5v-.5a.5.5 0 0 0-.5-.5H3z"></path>
							</svg></a>' ;
			$tabla1->cargar($i,2,$editar);
			$tabla1->cargar($i,3,$row->fecha);
			$tabla1->cargar($i,4,$row->nombre);
			$tabla1->cargar($i,5,$row->apellido);
			$tabla1->cargar($i,6,$row->direccion);
			$tabla1->cargar($i,7,$row->horario);			
			}
			$tabla1->graficar("table thead-light");
		}


	  $this->pagina->pie();
	}

 }
 


