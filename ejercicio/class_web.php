<?php

class pagina_Web
{
	public $titulo;

	public function __construct($titulo = "Titulo por defecto")
	{
	   $this->setTitulo($titulo); 
	}

	private function setTitulo($titulo)
	{
		$this->titulo = $titulo;
	}

	public function getTitulo()
	{
		return $this->titulo;
	}

	public function cabecera()
	{ ?>
	<!doctype html>
	<html lang="es">
	<head>
	
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<title><?php echo $this->titulo; ?></title>
	</head>
	<?php
	}

	public function cuerpo()
	{
		echo '<body><div class="container">';
	} 

	public function pie ( )
	{
		echo ("</div></body></html>") ;
	}

	public function mostrar_pagina()
	{
		echo $this->cabecera() ;
		echo $this->cuerpo() ;
		echo $this->pie();
	}

}

class pagina_Web_formulario extends pagina_Web
{ 
	public $action;

	public function formulario_inicio($accion,$method="POST")
	{
		echo "<h6 class='text-white bg-dark'>$this->titulo</h6>";
		echo ("<form class=\"form\" action=\"$accion\" method=\"$method\">");
	}
	public function form_radio($name, $value, $opt, $checked="")
    {
        echo "<div class=\"form-check\">";
        echo "<input type=\"radio\" class=\"form-check-input mb-3\" name=\"$name\" id=\"$opt\" value=\"$value\" $checked />";
        echo "<label style=\"text-transform: capitalize;\" for=\"$opt\" class=\"form-check-label\" >$opt</label>";
        echo "<div/>";
    }

	public function formulario_fin()
	{
		echo ("</form>");
	}

	public function formulario_label($texto,$input){
		echo "<label for=\"$input\">$texto</label>";
	}

	public function formulario_caja_texto($texto,$nombre,$required="",$value="",$type="text")
	{
		echo "<div class=\"col-sm-8\">";
		$this->formulario_label($texto,$nombre);
		echo "<input type=\"$type\" class=\"col-sm-8\" name=\"$nombre\" value=\"$value\"";
		if ($required == "X") {
			echo " required ";
		}
		echo "><br>";
		echo "</div>";
	}
	
	public function formulario_boton($value)
	{
		echo ("<input type=\"submit\" name=\"Submit\" value = \"$value\" class=\"btn btn-secondary btn-lg btn-block\">" ) ;
	}

}

 