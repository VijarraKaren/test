<?php
require_once('class_repartos.php');

$reparto = new reparto();

if (isset($_POST['fecha']) && 
	isset($_POST['nombre']) && 
	isset($_POST['apellido']) &&
	isset($_POST['direccion']) &&
	isset($_POST['horario'])) {

	$fecha = $_POST['fecha'];
	$nombre = $_POST['nombre'];
	$apellido = $_POST['apellido'];
	$direccion = $_POST['direccion'];	
	$horario = $_POST['horario'];	
	if (isset($_GET['id'])) {
		 $id = $_GET['id'];
		 $reparto->update($id,$fecha,$nombre,$apellido,$direccion,$horario);
	}
	else{
		$reparto->insert($fecha,$nombre,$apellido,$direccion,$horario);
	 }
  
}

$reparto->mostrar_tabla();

?>